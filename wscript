from waflib import Logs
from waflib.Tools import waf_unit_test
from waflib.Context import Context

APPNAME = 'cilt'
VERSION = '0.1'

top = '.'
out = 'build'


def gtest_results(bld):
    lst = getattr(bld, 'utest_results', [])
    if not lst:
        return
    for (f, code, out, err) in lst:
        # if not code:
        #     continue

        output = str(out).splitlines()
        for i, line in enumerate(output):
            if '[ RUN      ]' in line and code:
                i += 1
                if '    OK ]' in output[i]:
                    continue
                while not '[ ' in output[i]:
                    Logs.warn('%s' % output[i])
                    i += 1
            elif ' FAILED  ]' in line and code:
                Logs.error('%s' % line)
            elif ' PASSED  ]' in line:
                Logs.info('%s' % line)


def options(opt):
    opt.load('compiler_cxx')
    opt.load('waf_unit_test')
    opt.add_option('--tests', action='store_true',
                   help='compile tests', default=False)
    opt.add_option('--alltests', action='store_false',
                   default=True, help='Exec all unit tests', dest='no_tests')


def configure(cfg):
    cfg.load('compiler_cxx')

    if cfg.options.tests:
        cfg.recurse('3rdparty/gtest', 'configure')
        cfg.env.tests = True
        cfg.env.append_value('INCLUDES',
                             cfg.path.find_dir('3rdparty/gtest/include').abspath())
        cfg.check(lib='pthread', uselib_store='PTHREAD')
    else:
        cfg.env.tests = False

    cfg.env.append_value('INCLUDES', [cfg.path.find_dir('src/c').abspath(),
                                      cfg.path.find_dir('src/cpp').abspath()])


def build(bld):
    bld.recurse('src')

    if bld.env.tests:
        bld.recurse(['3rdparty/gtest', 'test'])

        bld.add_post_fun(gtest_results)
